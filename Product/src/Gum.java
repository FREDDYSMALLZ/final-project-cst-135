import java.util.Random;

public class Gum extends Snack implements Celebrator{

		public Gum(double calories, double quanitySize,
				java.util.Date manufacturingAndExpiryDate) {
			super(calories, quanitySize, manufacturingAndExpiryDate);
				}
		
		String name;
		double calories;
		double size;
		String manufacturingAndExpiryDate;
		double cost;
		double weight;
		String category;
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public double getCalories() {
			return calories;
		}
		public void setCalories(double calories) {
			this.calories = calories;
		}
		public double getSize() {
			return size;
		}
		public void setSize(double size) {
			this.size = size;
		}
		public String getManufacturingAndExpiryDate1() {
			return manufacturingAndExpiryDate;
		}
		public void setManufacturingAndExpiryDate(String manufacturingAndExpiryDate) {
			this.manufacturingAndExpiryDate = manufacturingAndExpiryDate;
		}
		public double getCost() {
			return cost;
		}
		public void setCost(double cost) {
			this.cost = cost;
		}
		public double getWeight() {
			return weight;
		}
		public void setWeight(double weight) {
			this.weight = weight;
		}
		public String getCategory() {
			return category;
		}
		public void setCategory(String category) {
			this.category = category;
		}
		@Override
		public double getQuantitySize() {
						return 0;
		}
		@Override
		public void setQuantitySize(double quantitySize) {
						
		}
		@Override
		public java.util.Date getManufacturingAndExpiryDate() {
						return null;
		}
		@Override
		public void setManufacturingAndExpiryDate(java.util.Date manufacturingAndExpiryDate) {
						
		}
		
public String celebrate() 
{
	int r = new Random().nextInt(4);
	String message = "";
	switch(r){
case 0: message = " Great Choice";break;
case 1: message = " I would have selected the other one";break;
case 2: message = " Wow realy!";break;
case 3: message = " Enjoy your day";break;
case 4: message = " Be blessed";break;
	}

	return message;
	}
}
	

