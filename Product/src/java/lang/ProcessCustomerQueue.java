package java.lang;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ProcessCustomerQueue {

	public static void main(String[] args) {
		String fileName = "Customer Queue";
		File file = new File(fileName);
		try {
			Scanner inputStream = new Scanner(file);
			while (inputStream.hasNext()){
				String data = inputStream.next();
				System.out.println(data);
				
			}
			inputStream.close();
		}catch (FileNotFoundException e) {
						e.printStackTrace();
		}
	}

}
